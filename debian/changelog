wsdd (2:0.8-2) unstable; urgency=medium

  [ Alessandro Astone ]
  * Enable regression tests at build time (LP: #2070057)
  * Create regression autopkgtests (LP: #2070186)
    This test suite comes from the upstream project.
    It ensures basic functionality of the wsdd daemon including:
    - startup
    - shutdown
    - sending commands
  * Upgrade debhelper compat version to 13 (LP: #2070263)
  * Fix escape sequence in man page (LP: #2070263)
  * Add uscan watch file (LP: #2070264)

  [ Matthew Grant ]
  * Add firewalld rules files. (Closes: #1073265)

 -- Matthew Grant <matt@mattgrant.net.nz>  Thu, 27 Jun 2024 12:51:40 +1200

wsdd (2:0.8-1) unstable; urgency=medium

  * New upstream version 0.8 (Closes: #1070707)
  * Build with debhelper 13.11.8, moves /lib/systemd to /usr/lib/systemd
    (Closes:  #1071908)
  * Correct VCS urls in debian/control (Closes: #1067718)

 -- Matthew Grant <matt@mattgrant.net.nz>  Tue, 28 May 2024 11:16:21 +1200

wsdd (2:0.7.1-5) unstable; urgency=medium

  * Create wsdd-server package

 -- Matthew Grant <matt@mattgrant.net.nz>  Mon, 19 Feb 2024 13:36:25 +1300

wsdd (2:0.7.1-4) unstable; urgency=medium

  * Move wsdd to /usr/bin, rename man page. (Closes: #1037914, #1064057)

 -- Matthew Grant <matt@mattgrant.net.nz>  Sat, 17 Feb 2024 18:21:07 +1300

wsdd (2:0.7.1-3) unstable; urgency=medium

  * New upstream version 0.7.1
  * Symlink /usr/sbin/wsdd to /usr/bin/wsdd (Closes: #1037914)
  * Update debian/patches for new release 0.7.1
    Main improvement, manpage moved upstream.

 -- Matthew Grant <matt@mattgrant.net.nz>  Sat, 17 Feb 2024 15:35:50 +1300

wsdd (2:0.7.0-2) unstable; urgency=medium

  * Source only upload for testing etc.

 -- Matthew Grant <matt@mattgrant.net.nz>  Thu, 14 Apr 2022 12:24:21 +1200

wsdd (2:0.7.0-1~bpo11+1) bullseye-backports; urgency=medium

  * Rebuild for bullseye-backports.

 -- Matthew Grant <matt@mattgrant.net.nz>  Fri, 11 Feb 2022 18:30:03 +1300

wsdd (2:0.7.0-1) unstable; urgency=medium

  * Initial release (Closes: #1005270)
  * Use epoch 2 as there are currently popular non-standard debian packages
    out in the wild, mainly from https://pkg.ltec.ch/public/pool/main/w/wsdd/

 -- Matthew Grant <matt@mattgrant.net.nz>  Fri, 11 Feb 2022 12:34:36 +1300
