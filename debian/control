Source: wsdd
Section: net
Priority: optional
Maintainer: Matthew Grant <matt@mattgrant.net.nz>
Build-Depends:
 debhelper (>= 13.11.8~bpo12+1),
 debhelper-compat (= 13),
 python3-all (>= 3.11),
 dh-python,
 python3-setuptools (>= 3.11),
 python3 (>= 3.11)
X-Python3-Version: >= 3.11
Standards-Version: 4.6.1
Homepage: https://github.com/christgau/wsdd
Vcs-Browser: https://salsa.debian.org/grantma/wsdd
Vcs-Git: https://salsa.debian.org/grantma/wsdd.git
Rules-Requires-Root: no

Package: wsdd
Architecture: all
Depends: python3 (>= 3.11),
 ${misc:Depends}
Description: Python Web Services Discovery Daemon, Windows Net Browsing
 This daemon is used to announce Linux Hosts to Windows 7+ computers for
 use in their File Manager network browsing, by using the Windows
 Services Discovery Protocol.
 .
 This protocol is a local network segment procotol, which is multicasted
 on udp/3072, and incoming on tcp/5357 on the 239.255.255.250/ff02::c
 multicast addresses.  It DOES have security issues, but it is designed
 for use in a trusted environment inside a firewall.
 .
 Install only this if running Gnome desktop, or GVFS and not announcing
 well known File shares.  See wsdd-server for working with Samba servers.

Package: wsdd-server
Architecture: all
Depends: python3 (>= 3.11), wsdd (= ${binary:Version}),
 ${misc:Depends}
Description: Python Web Services Discovery Daemon, Windows Net serving
 Run wsdd as a server announcing the Host to network.  Install if
 running Samba smbd for file shares.
 .
 The wsdd daemon is used to announce Linux Hosts to Windows 7+
 computers for use in their File Manager network browsing, by using the
 Windows Services Discovery Protocol.
 .
 This protocol is a local network segment procotol, which is multicasted
 on udp/3072, and incoming on tcp/5357 on the 239.255.255.250/ff02::c
 multicast addresses.  It DOES have security issues, but it is designed
 for use in a trusted environment inside a firewall.
 .
 Its quite useful for Samba, taking over from WINS and the Samba nmbd
 daemon.  Installing this restores the Network browsing functionality to
 Windows 7+ Samba clients.
